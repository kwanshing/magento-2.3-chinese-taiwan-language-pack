## Magento 2 Chinese Taiwan Language Pack



Read more [Magento 2 Chinese Taiwan Language Pack](https://www.mageplaza.com/magento-2-chinese-taiwan-language-pack.html)

![Mageplaza Chinese Taiwan language pack](https://cdn3.mageplaza.com/media/general/qjWPj1W.png)

## Overview

1. Language Package Process
2. Install Chinese Taiwan Language Pack
3. How to active Chinese Taiwan language pack
4. How to contribute
5. Supported Magento versions
6. Notes
7. Language package authors

## 1. Language Package Process

This is status of Chinese Taiwan Language Pack, you can see how many percentage of this project has been done.

![language pack](https://progress-bar.dev/11/?title=translated)

It is not fully translated? Feel free to contribute:
- [On Crowdin](https://crowdin.com/project/magento-2): It takes time to approve your contribution by Magento team.
- [On Github](https://github.com/mageplaza/magento-2-chinese-taiwan-language-pack/blob/master/HOW-TO-CONTRIBUTE.md): It's faster, our team will approve it after you send pull request.


Find other [language packs here](https://www.mageplaza.com/kb/magento-2-language-pack/)

## 2. How to Install Chinese Taiwan Language Pack

There are 3 different methods to install this language pack.

### ✓ Method #1. Composer method (Recommend)
Install the Chinese Taiwan language pack via composer is never easier.

**Install Chinese Taiwan pack**:

```
composer require kwanshing/magento-2-chinese-taiwan-language-pack:dev-master
php bin/magento setup:static-content:deploy zh_Hant_TW
php bin/magento indexer:reindex
php bin/magento cache:clean
php bin/magento cache:flush

```


**Update  Chinese Taiwan pack**:

```
composer update kwanshing/magento-2-chinese-taiwan-language-pack:dev-master
php bin/magento setup:static-content:deploy zh_Hant_TW
php bin/magento indexer:reindex
php bin/magento cache:clean
php bin/magento cache:flush

```

#### Authentication required (If any)

![Authentication required](https://cdn.mageplaza.com/media/general/dmryiPk.png)

If you have not added this authentication, you can follow [this guide](http://devdocs.magento.com/guides/v2.0/install-gde/prereq/connect-auth.html)

Or use these keys:

```
Public Key: c7af1bfc9352e9c986637eec85ed53af
Private Key: 17e1b72ea5f0b23e9dbfb1f68dc12b53
```



### ✓ Method #2. Copy & Paste method (Not recommended)

This method suitable for non-technical people such as merchants. Just download the package then flush cache.

**Overview**

- Step 1: Download the Chinese Taiwan language pack
- Step 2: Unzip Chinese Taiwan pack
- Step 3: Flush Magento 2 Cache

#### Step 1 : Download the Chinese Taiwan language pack

You can download the language pack from above link

#### Step 2: Unzip Chinese Taiwan pack

Unzip the Chinese Taiwan language pack to Magento 2 root folder. In this guide, we extract to `/var/www/html/`
Your Magento 2 root folder can be: `/home/account_name/yourstore.com/public_html/`

```
unzip master.zip app/i18n/Mageplaza/
```

Rename folder `magento-2-chinese-taiwan-language-pack` to `zh_hant_tw`.


You also can unzip locally and upload them to Magento 2 root folder.

#### Step 3: Flush Magento 2 Cache

Follow this guide to [Flush Cache on your Magento 2 store](https://www.mageplaza.com/kb/how-flush-enable-disable-cache.html)